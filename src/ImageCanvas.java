import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;
import javax.swing.JViewport;

public class ImageCanvas extends JPanel {
	protected BufferedImage image,temp;
	protected double zoomFactor = 1.0;
	double scale = 1.0;
	private double zoom = 1.0;

	AffineTransform tx = new AffineTransform();
	public static final double SCALE_STEP = 0.1d;
	private double previousZoom = zoom;
	protected Point2D mousePosition = new Point2D.Double(0, 0);
	private Dimension initialSize;
	private Point origin=new Point();
	
	public JViewport getParentViewPort() {
		return parentViewPort;
	}

	public void setParentViewPort(JViewport parentViewPort) {
		this.parentViewPort = parentViewPort;
	}

	protected JViewport parentViewPort = null;

	ImageCanvas() {
		
		origin.setLocation(0, 0);
		
		this.addMouseWheelListener(new MouseWheelListener() {
			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				/*
				 * int notch=e.getWheelRotation();
				 * 
				 * double newZoomFactor=Math.max((zoomFactor-(notch*0.2)), .25);
				 * 
				 * mousePosition=e.getPoint(); if (newZoomFactor!=zoomFactor &&image!=null) {
				 * 
				 * zoomFactor=newZoomFactor; repaint(); if(parentViewPort!=null) {
				 * parentViewPort.setViewPosition(new
				 * Point((int)(e.getX()*zoomFactor),(int)(e.getY()*zoomFactor))); //
				 * parentViewPort.setViewSize(new
				 * Dimension((int)(image.getWidth()*zoomFactor),(int)(image.getHeight()*
				 * zoomFactor))); System.out.println(e.getPoint()); }
				 * 
				 * //
				 * image=image.getScaledInstance((int)(image.getWidth()*zoomFactor),(int)(image.
				 * getHeight()*zoomFactor),image.SCALE_FAST )); //RescaleOp scaler=new
				 * RescaleOp((float)(image.getWidth()*zoomFactor),(float)(image.getHeight()*
				 * zoomFactor));
				 */

				if (image != null) {
					/*
					 * double zoomFactor = - SCALE_STEP*e.getPreciseWheelRotation()*zoom;
					 * 
					 * zoom = Math.abs(zoom + zoomFactor); //Here we calculate new size of canvas
					 * relative to zoom. Dimension d = new Dimension( (int)(initialSize.width*zoom),
					 * (int)(initialSize.height*zoom)); setPreferredSize(d); setSize(d); validate();
					 * 
					 * 
					 * Rectangle size = getBounds(); Rectangle visibleRect = getVisibleRect();
					 * double scrollX = size.getCenterX(); double scrollY = size.getCenterY();
					 * 
					 * scrollX = e.getX()/previousZoom*zoom - (e.getX()-visibleRect.getX()); scrollY
					 * = e.getY()/previousZoom*zoom - (e.getY()-visibleRect.getY());
					 * 
					 * 
					 * visibleRect.setRect(scrollX, scrollY, visibleRect.getWidth(),
					 * visibleRect.getHeight()); scrollRectToVisible(visibleRect);
					 */

					scale += (.1 * e.getWheelRotation());
					scale = Math.max(0.1, scale);
					Point2D p = e.getPoint();
					//p.setLocation(p.getX()*(1-scale), p.getY()*(1-scale));
					  //temp =resize(image,(int)(image.getWidth()*scale),(int)(image.getHeight()*scale));
					//AffineTransformOp top=new AffineTransformOp(tx,AffineTransformOp.TYPE_BICUBIC);
					//top.filter(image, temp);
					//parentViewPort.setViewSize(new Dimension((int)(image.getWidth()),(int)(image.getHeight())));
					//parentViewPort.setViewPosition(p);
					tx.setToIdentity();
					
					p.setLocation((e.getX()/scale)+(origin.x/scale), (e.getY()/scale)+(origin.y/scale));
					tx.translate(p.getX(), p.getY());
					
					tx.scale(scale, scale);
					
					tx.translate(-p.getX(),-p.getY());
					
					
					
					
					revalidate();
					repaint();
					
					// previousZoom = zoom;
					origin.setLocation(e.getPoint());
				}
			}

		});
	}
	
	 private static BufferedImage resize(BufferedImage img, int height, int width) {
	        Image tmp = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
	        BufferedImage resized = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
	        Graphics2D g2d = resized.createGraphics();
	        g2d.drawImage(tmp, 0, 0, null);
	        g2d.dispose();
	        return resized;
	    }
	 
	public BufferedImage getImage() {
		return image;
	}

	public void setImage(BufferedImage image) {
		this.image = image;
		initialSize = (new Dimension((int) (image.getWidth() * 100), (int) (image.getHeight() * 100)));
	}

	public double getZoomFactor() {
		return zoomFactor;
	}

	public void setZoomFactor(double zoomFactor) {
		this.zoomFactor = zoomFactor;
	}

	public Dimension getPreferredSize() {
		if (image != null) {
			int w = (int) (scale* image.getWidth()), h = (int) ( scale*image.getHeight());
			return new Dimension(w, h);
		}
		return new Dimension(400, 400);
	}

	@Override
	public void paintComponent(Graphics g) {

		Graphics2D g2d = (Graphics2D) g;
		super.paintComponent(g2d);
		if (image != null) {
			// zoomFactor=1;
			int newImageWidth = (int) (image.getWidth() * zoomFactor);
			int newImageHeight = (int) (image.getHeight() * zoomFactor);
			// BufferedImage resizedImage = new BufferedImage(newImageWidth ,
			// newImageHeight, image.getType());
			// Graphics2D g3 = resizedImage.createGraphics();

			// g2d.drawImage(image,0,0, newImageWidth , newImageHeight , null);

			// Zoom graphics
			//g2d.scale(zoom, zoom);

			// translate graphics to be always in center of the canvas
			Rectangle size = getBounds();
			// double tx = ((size.getWidth() - image.getWidth() * zoom) / 2) / zoom;
			// double ty = ((size.getHeight() - image.getHeight() * zoom) / 2) / zoom;
			//g2d.translate(tx, ty);
			g2d.setTransform(tx);
		
			g2d.drawImage(image, null, 0, 0);
			g2d.dispose();

		} else {
			// System.out.println("Paint");
			// g2d.fillRect(0, 0, 60, 60);
		}

	}
	
	 private Point2D getTranslatedPoint(double panelX, double panelY) {
         
	        Point2D point2d = new Point2D.Double(panelX, panelY);
	        try {
	            return tx.inverseTransform(point2d, null);
	        } catch (NoninvertibleTransformException ex) {
	            ex.printStackTrace();
	            return null;
	        }
	         
	    }
}
