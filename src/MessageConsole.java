import java.awt.Color;
import java.awt.Component;
import java.awt.TextArea;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
		 
		
		@SuppressWarnings("serial")
		public class MessageConsole extends JTextPane  {
		    private JTextPane textPane;
		    private Document doc;
		     private Component parent;
		     private int lastLineIndex;
		     
		    public MessageConsole() {
		    	 this.textPane=this;
		    	 this.doc=this.getDocument();
		    }
		    
		    public MessageConsole(Component parent) {
		    	 this.textPane=this;
		    	 this.doc=this.getDocument();
		    	 this.parent=parent;
		    }
		 


			public void ConsolePrintLn(String in) {
		    	append("\n"+in,Color.black,false);
		    	//textPane.update(textPane.getGraphics());
		    	//parent.update(getGraphics());
		    }
		    
		    public void ConsolePrintLn(long in) {
		    	append("\n"+in,Color.black,false);
		    	//textPane.update(textPane.getGraphics());
		    	//parent.update(getGraphics());
		    }
		    

		    public void ConsolePrintLn(int in) {
		    	append("\n"+in,Color.black,false);
		    //	textPane.update(textPane.getGraphics());
		    //	parent.update(getGraphics());
		    }
		    
		    public void ConsoleError(String in) {
		    	append("\n"+in,Color.red,false);
		    	//textPane.update(textPane.getGraphics());
		    	//System.out.println(parent.getName());
		    	//parent.update(getGraphics());
		    }
		    
		    public void ConsoleTextUpdate(String in) {
		    	append("\n"+in,Color.black,true);
		    	
		    }
		    
		    public void setParent(Component parent) {
		    	this.parent=parent;
		    }
		    
		    
		    private void append(String in,Color textColor,boolean replace) {
		    	try {
		    		 SimpleAttributeSet sas = new SimpleAttributeSet ();
		    		 sas.addAttribute(StyleConstants.Foreground, textColor);
		    		
		    		 if(replace) {
		    			 doc.remove( lastLineIndex,doc.getLength()-lastLineIndex);
		    			
		    			 doc.insertString(doc.getLength(), in,sas);
		    		 }
		    		 else {
		    			 lastLineIndex=doc.getLength();
		    			 doc.insertString(doc.getLength(), in,sas);
		    		 }
					
				} catch (BadLocationException e) {
					
					e.printStackTrace();
					
				}
		    }
		    

		}