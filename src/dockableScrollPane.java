import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.*;
import javax.swing.text.DefaultCaret;

import com.javadocking.dockable.DraggableContent;
import com.javadocking.drag.DragListener;

public class dockableScrollPane extends JPanel implements DraggableContent {

	private JLabel label; 
	private MessageConsole mc;
	public dockableScrollPane(String text,MessageConsole mc)
	{
		super(new BorderLayout());
		this.mc=mc;
		// The panel.
		setMinimumSize(new Dimension(80,80));
		setPreferredSize(new Dimension(150,150));
		setBackground(Color.white);
		setBorder(BorderFactory.createLineBorder(Color.lightGray));
		
		// The label.
		label = new JLabel(text);
		label.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		//add(label);
		
		

		DefaultCaret caret = (DefaultCaret) mc.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		mc.setAutoscrolls(true);
		JScrollPane consoleScr = new JScrollPane(mc);
		consoleScr.setName("consoleScr");
		mc.setEditable(false);
		mc.setParent(consoleScr);
		mc.setSize(100, 100);
		mc.setVisible(true);
		
		consoleScr.setAutoscrolls(true);
		consoleScr.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		
		

		//consoleScr.setPreferredSize(new Dimension(100,100));
		//consoleScr.setLocation(30, 30);
		// win.add(p);
		consoleScr.setVisible(true);
		
		add(consoleScr,BorderLayout.CENTER);

	}
	@Override
	public void addDragListener(DragListener dragListener) {
		addMouseListener(dragListener);
		addMouseMotionListener(dragListener);

	}

}
