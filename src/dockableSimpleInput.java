import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.*;

import com.javadocking.dockable.DraggableContent;
import com.javadocking.drag.DragListener;

public class dockableSimpleInput extends JPanel implements DraggableContent, ActionListener {

	private JTextField edgeDistanceTxt, sigmaCannyTxt, kernelCannyTxt, sigmaSobelTxt,kernelSobelTxt;
	private RotoScape panel;
	private MessageConsole mc;
	private String page;

	public dockableSimpleInput(RotoScape panel, MessageConsole mc, JTextField edgeDistanceTxt, JTextField sigmaCannyTxt,
			JTextField kernelCannyTxt,JTextField sigmaSobelTxt,JTextField kernelSobelTxt) {
		super(new CardLayout());
		this.page="Simple";
		this.edgeDistanceTxt = edgeDistanceTxt;
		this.kernelCannyTxt = kernelCannyTxt;
		this.sigmaCannyTxt = sigmaCannyTxt;
		this.sigmaSobelTxt=sigmaSobelTxt;
		this.kernelSobelTxt=kernelSobelTxt;
		this.panel = panel;
		this.mc = mc;

		/*
		 * Simple Input ===============================================
		 */
		JPanel simpleInput = new JPanel();
		simpleInput.setLayout(new BoxLayout(simpleInput, BoxLayout.Y_AXIS));
		simpleInput.setBorder(BorderFactory.createEmptyBorder(20, 10, 40, 10));

		JLabel PanelLbl_Simple = new JLabel("Simple Edge Detection");

		edgeDistanceTxt.setPreferredSize(new Dimension(100, 40));
		edgeDistanceTxt.setMinimumSize(new Dimension(100, 40));
		edgeDistanceTxt.setMaximumSize(new Dimension(100, 40));
		edgeDistanceTxt.setForeground(Color.black);
		edgeDistanceTxt.setAlignmentX(Component.CENTER_ALIGNMENT);
		edgeDistanceTxt.setAlignmentY(Component.TOP_ALIGNMENT);

		JButton renderBtn_Simple = new JButton("Apply");
		renderBtn_Simple.setSize(80, 50);
		renderBtn_Simple.setEnabled(true);
		renderBtn_Simple.setVisible(true);
		renderBtn_Simple.addActionListener(this);

		JSeparator ss = new JSeparator(SwingConstants.HORIZONTAL);
		ss.setBackground(Color.black);
		ss.setAlignmentX(Component.CENTER_ALIGNMENT);
		ss.setPreferredSize(new Dimension(800, 30));
		ss.setMaximumSize(new Dimension(800, 30));

		simpleInput.add(PanelLbl_Simple);
		simpleInput.add(ss);
		// simpleInput.add(Box.createVerticalStrut(20));
		simpleInput.add(edgeDistanceTxt);
		simpleInput.add(Box.createVerticalStrut(40));
		simpleInput.add(renderBtn_Simple);

		/*
		 * Sobel Input ===============================================
		 */
		JPanel sobelInput = new JPanel();
		JLabel PanelLbl_Sobel = new JLabel("Sobel Edge Detection");

		JSeparator so = new JSeparator(SwingConstants.HORIZONTAL);
		so.setBackground(Color.black);
		so.setAlignmentX(Component.CENTER_ALIGNMENT);
		so.setPreferredSize(new Dimension(800, 30));
		so.setMaximumSize(new Dimension(800, 30));

		
		sigmaSobelTxt.setPreferredSize(new Dimension(100, 40));
		sigmaSobelTxt.setMinimumSize(new Dimension(100, 40));
		sigmaSobelTxt.setMaximumSize(new Dimension(100, 40));
		sigmaSobelTxt.setForeground(Color.black);
		sigmaSobelTxt.setAlignmentX(Component.CENTER_ALIGNMENT);
		//sigmaSobelTxt.setAlignmentY(Component.TOP_ALIGNMENT);

		kernelSobelTxt.setPreferredSize(new Dimension(100, 40));
		kernelSobelTxt.setMinimumSize(new Dimension(100, 40));
		kernelSobelTxt.setMaximumSize(new Dimension(100, 40));
		kernelSobelTxt.setForeground(Color.black);
		kernelSobelTxt.setAlignmentX(Component.CENTER_ALIGNMENT);
	//	kernelSobelTxt.setAlignmentY(Component.TOP_ALIGNMENT);

		
		JButton renderBtn_Sobel = new JButton("Apply");
		renderBtn_Sobel.setSize(80, 50);
		renderBtn_Sobel.setEnabled(true);
		renderBtn_Sobel.setVisible(true);
		renderBtn_Sobel.addActionListener(this);

		sobelInput.add(PanelLbl_Sobel);
		sobelInput.add(so);
		sobelInput.add(sigmaSobelTxt);
		sobelInput.add(Box.createHorizontalStrut(30));
		sobelInput.add(kernelSobelTxt);
		sobelInput.add(Box.createHorizontalStrut(40));
		sobelInput.add(renderBtn_Sobel, BorderLayout.SOUTH);
		
		
		
		/*
		 * Canny Input ===============================================
		 */
		JPanel cannyInput = new JPanel();
		cannyInput.setLayout(new BoxLayout(cannyInput, BoxLayout.Y_AXIS));
		cannyInput.setBorder(BorderFactory.createEmptyBorder(20, 10, 40, 10));

		JLabel PanelLbl_Canny = new JLabel("Canny Edge Detection");

		sigmaCannyTxt.setPreferredSize(new Dimension(100, 40));
		sigmaCannyTxt.setMinimumSize(new Dimension(100, 40));
		sigmaCannyTxt.setMaximumSize(new Dimension(100, 40));
		sigmaCannyTxt.setForeground(Color.black);
		sigmaCannyTxt.setAlignmentX(Component.CENTER_ALIGNMENT);
		sigmaCannyTxt.setAlignmentY(Component.TOP_ALIGNMENT);

		kernelCannyTxt.setPreferredSize(new Dimension(100, 40));
		kernelCannyTxt.setMinimumSize(new Dimension(100, 40));
		kernelCannyTxt.setMaximumSize(new Dimension(100, 40));
		kernelCannyTxt.setForeground(Color.black);
		kernelCannyTxt.setAlignmentX(Component.CENTER_ALIGNMENT);
		kernelCannyTxt.setAlignmentY(Component.TOP_ALIGNMENT);

		JButton renderBtn_Canny = new JButton("Apply");
		renderBtn_Canny.setSize(80, 50);
		renderBtn_Canny.setEnabled(true);
		renderBtn_Canny.setVisible(true);
		renderBtn_Canny.addActionListener(this);

		JSeparator sc = new JSeparator(SwingConstants.HORIZONTAL);
		sc.setBackground(Color.black);
		sc.setAlignmentX(Component.CENTER_ALIGNMENT);
		sc.setPreferredSize(new Dimension(800, 30));
		sc.setMaximumSize(new Dimension(800, 30));

		cannyInput.add(PanelLbl_Canny);
		cannyInput.add(sc);
		// simpleInput.add(Box.createVerticalStrut(20));
		cannyInput.add(sigmaCannyTxt);
		cannyInput.add(Box.createVerticalStrut(30));
		cannyInput.add(kernelCannyTxt);
		cannyInput.add(Box.createVerticalStrut(40));
		cannyInput.add(renderBtn_Canny);

		this.add(simpleInput, "Simple");
		this.add(sobelInput, "Sobel");
		this.add(cannyInput, "Canny");
		// The panel.
		setMinimumSize(new Dimension(80, 80));
		setPreferredSize(new Dimension(150, 150));
		setBackground(Color.white);
		setBorder(BorderFactory.createLineBorder(Color.lightGray));

		// edgeDistanceTxt.setBounds(50, 100, 100, 30);
		FocusListener edgeListener = new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {
				edgeDistanceTxt.setText("");
				edgeDistanceTxt.setForeground(Color.black);

			}

			@Override
			public void focusLost(FocusEvent arg0) {
				if (edgeDistanceTxt.getText().isEmpty()) {
					edgeDistanceTxt.setForeground(Color.lightGray);
					edgeDistanceTxt.setText("Edge distance");

				}

			}
		};

		FocusListener sigmaCannyListener = new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {
				sigmaCannyTxt.setText("");
				sigmaCannyTxt.setForeground(Color.black);

			}

			@Override
			public void focusLost(FocusEvent arg0) {
				if (sigmaCannyTxt.getText().isEmpty()) {
					sigmaCannyTxt.setForeground(Color.lightGray);
					sigmaCannyTxt.setText("0.0");

				} else {
					try {
						Double.parseDouble(sigmaCannyTxt.getText());
					} catch (Exception e1) {
						mc.ConsoleError("Error: Invalid Input, Please Input Double");
						sigmaCannyTxt.setText("0.0");
					}

				}

			}
		};

		FocusListener kernelCannyListener = new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {
				kernelCannyTxt.setText("");
				kernelCannyTxt.setForeground(Color.black);

			}

			@Override
			public void focusLost(FocusEvent arg0) {
				if (kernelCannyTxt.getText().isEmpty()) {
					kernelCannyTxt.setForeground(Color.lightGray);
					kernelCannyTxt.setText("Odd number");

				} else if (Integer.parseInt(kernelCannyTxt.getText()) % 2 != 0) {
					try {
						Integer.parseInt(kernelCannyTxt.getText());
					} catch (Exception e1) {
						mc.ConsoleError("Error: Invalid Input, Please Input an Odd Number");
						kernelCannyTxt.setText("Odd number");
					}

				} else {
					mc.ConsoleError("Error: Invalid Input, Please Input an Odd Number");
					kernelCannyTxt.setText("Odd number");
				}

			}

		};
		
		
		
		
		FocusListener sigmaSobelListener = new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {
				sigmaSobelTxt.setText("");
				sigmaSobelTxt.setForeground(Color.black);

			}

			@Override
			public void focusLost(FocusEvent arg0) {
				if (sigmaSobelTxt.getText().isEmpty()) {
					sigmaSobelTxt.setForeground(Color.lightGray);
					sigmaSobelTxt.setText("0.0");

				} else {
					try {
						Double.parseDouble(sigmaSobelTxt.getText());
					} catch (Exception e1) {
						mc.ConsoleError("Error: Invalid Input, Please Input Double");
						sigmaSobelTxt.setText("0.0");
					}

				}

			}
		};

		FocusListener kernelSobelListener = new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {
				kernelSobelTxt.setText("");
				kernelSobelTxt.setForeground(Color.black);

			}

			@Override
			public void focusLost(FocusEvent arg0) {
				if (kernelSobelTxt.getText().isEmpty()) {
					kernelSobelTxt.setForeground(Color.lightGray);
					kernelSobelTxt.setText("Odd number");

				} else if (Integer.parseInt(kernelSobelTxt.getText()) % 2 != 0) {
					try {
						Integer.parseInt(kernelSobelTxt.getText());
					} catch (Exception e1) {
						mc.ConsoleError("Error: Invalid Input, Please Input an Odd Number");
						kernelSobelTxt.setText("Odd number");
					}

				} else {
					mc.ConsoleError("Error: Invalid Input, Please Input an Odd Number");
					kernelSobelTxt.setText("Odd number");
				}

			}

		};

		edgeDistanceTxt.addFocusListener(edgeListener);
		sigmaCannyTxt.addFocusListener(sigmaCannyListener);
		kernelCannyTxt.addFocusListener(kernelCannyListener);
		sigmaSobelTxt.addFocusListener(sigmaSobelListener);
		kernelSobelTxt.addFocusListener(kernelSobelListener);

	}

	
	public void setPage(String page) {
		this.page = page;
		CardLayout cl = (CardLayout) (this.getLayout());
		cl.show(this, page);
	}

	private int getEdgeValue() {
		try {
			return Integer.parseInt(edgeDistanceTxt.getText());
		} catch (Exception e) {
			return 0;
		}
	}

	private double getSigmaCannyValue() {
		try {
			return Double.parseDouble(sigmaCannyTxt.getText());
		} catch (Exception e) {
			return 0.0;
		}

	}

	private int getKernelCannyValue() {

		try {
			return Integer.parseInt(kernelCannyTxt.getText());
		} catch (Exception e) {
			return 0;
		}

	}


	private double getSigmaSobelValue() {
		try {
			return Double.parseDouble(sigmaSobelTxt.getText());
		} catch (Exception e) {
			return 0.0;
		}

	}

	private int getKernelSobelValue() {

		try {
			return Integer.parseInt(kernelSobelTxt.getText());
		} catch (Exception e) {
			return 0;
		}

	}
	
	@Override
	public void addDragListener(DragListener dragListener) {
		addMouseListener(dragListener);
		addMouseMotionListener(dragListener);

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		try {
			if(this.page=="Sobel") {
			//panel.renderSobel(getSigmaSobelValue(),getKernelSobelValue());
				panel.updateRequestSobel(getSigmaSobelValue(), getKernelSobelValue());
			}
			else if(this.page=="Canny") {
				//panel.renderCanny(getSigmaCannyValue(),getKernelCannyValue(),1);
				panel.updateRequestCanny(getSigmaCannyValue(), getKernelCannyValue());
			}
			else if(this.page=="Simple") {
				//panel.renderSimple(getEdgeValue());
				panel.updateRequestSimple(getEdgeValue());
			}
		} catch (Exception e1) {

			mc.ConsoleError("Error: fill in all required feilds 2");
			e1.printStackTrace();
		}

	}

}
