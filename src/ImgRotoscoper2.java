import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.Callable;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.FileImageOutputStream;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.ImageOutputStream;

import org.bytedeco.javacv.FrameGrabber.Exception;
import org.bytedeco.opencv.opencv_core.*;
import org.bytedeco.opencv.opencv_imgproc.*;
import org.jcodec.api.SequenceEncoder;
import org.jcodec.api.awt.AWTSequenceEncoder;
import org.jcodec.codecs.h264.H264Encoder;

import com.google.common.io.Files;

import static org.bytedeco.opencv.global.opencv_core.*;
import static org.bytedeco.opencv.global.opencv_imgproc.*;
import static org.bytedeco.opencv.global.opencv_imgcodecs.*;

public class ImgRotoscoper2 extends rotoscopeUtil {

	ImgRotoscoper2(File in, MessageConsole mc, imagePreviewPanel previewPanel) throws Exception, IOException {
		super(in, mc, previewPanel);

	}

	public void nameOutputFile(double ed, int sharp) throws java.lang.Exception {

		mc.ConsolePrintLn(input.getPath());
		mc.ConsolePrintLn(input.getParentFile().toString());
		String parentdir = input.getParentFile().toString();
		String fileName = input.getName();
		fileName = fileName.substring(0, fileName.indexOf("."));
		mc.ConsolePrintLn(fileName);
		File fi = new File(parentdir + "\\" + fileName + "Roto.png");
		mc.ConsolePrintLn(fi.toString());

		mc.ConsolePrintLn("*Completed input");

		switch (detectionAlgorithm) {

		case "Simple":
			simpleImage(fi);
			break;

		case "Sobel":
			sobelImage(fi);
			break;

		case "Canny":
			cannyImage(fi);
			break;

		case "NoFilter": // just woke up like this
			noFilterImage();
			break;
		case "Open":
			openImage();
			break;

		default:
			mc.ConsoleError("Error: unknown algorithm type");

		}
	}

	public void simpleImage(File fi) {

		long StartTime = System.currentTimeMillis();

		Picture san = new Picture(outputImage[0]);
		san.edgeDetection2(this.edge);

		/*
		 * try { ImageIO.write(san.getBufferedImage(), "png", fi); } catch (IOException
		 * e) { // TODO Auto-generated catch block e.printStackTrace(); }
		 * 
		 * long EndTime = (System.currentTimeMillis()); float Sec = (EndTime -
		 * StartTime) / 1000f; float Min = Sec / 60f;
		 * mc.ConsolePrintLn("Operation Completed in " + (Min) + " min");
		 */

	}

	private void openImage() {
		try {
			this.outputImage = new BufferedImage[1];
			this.outputImage[0] = deepCopy(ImageIO.read(input));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private BufferedImage deepCopy(BufferedImage bi) {
		ColorModel cm = bi.getColorModel();
		boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
		WritableRaster raster = bi.copyData(null);
		return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
	}

	public void sobelImage(File fi) {

		long StartTime = System.currentTimeMillis();

		// san.canny(sigma, kernelSize,0.03,0.07,fi);
		Picture san = new Picture(outputImage[0]);
		san.guassianBlur(sigma, kernelSize);
		san.sobel();

		/*
		 * try { ImageIO.write(outputImage.getBufferedImage(), "png", fi); } catch
		 * (IOException e) { // TODO Auto-generated catch block e.printStackTrace(); }
		 * 
		 * long EndTime = (System.currentTimeMillis()); float Sec = (EndTime -
		 * StartTime) / 1000f; float Min = Sec / 60f;
		 * mc.ConsolePrintLn("Operation Completed in " + (Min) + " min");
		 */
		outputImage[0] = san.getBufferedImage();
	}

	public void cannyImage(File fi) {

		long StartTime = System.currentTimeMillis();

		Picture san = new Picture(outputImage[0]);
		san.canny(sigma, kernelSize, 0.03, 0.07, fi);
		// san.guassianBlur(sigma, kernelSize);
		// san.sobel();

		/*
		 * try { ImageIO.write(outputImage.getBufferedImage(), "png", fi); } catch
		 * (IOException e) { // TODO Auto-generated catch block e.printStackTrace(); }
		 */
		outputImage[0] = san.getBufferedImage();
		long EndTime = (System.currentTimeMillis());
		float Sec = (EndTime - StartTime) / 1000f;
		float Min = Sec / 60f;
		mc.ConsolePrintLn("Operation Completed in " + (Min) + " min");

	}

	public void noFilterImage() {
		try {
			outputImage[0] = ImageIO.read(input);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		try {
			System.out.println("Started background task: " + detectionAlgorithm);
			nameOutputFile(edge, sharp);
			// BufferedImage[] outarray=(BufferedImage[]) outbuf.toArray(new
			// BufferedImage[outbuf.size()-1]);
			previewPanel.setImage(outputImage);
			// previewPanel.setBackground(Color.red);
			previewPanel.repaint();

			System.out.println("Completed Task: " + detectionAlgorithm);

		} catch (java.lang.Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}