import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.basic.BasicSliderUI.TrackListener;
import javax.swing.plaf.metal.MetalSliderUI;
public class TimeLine extends JPanel {

	private JSlider timeLineSlider;
	private JTable  frameTable;
	private int variableWidth=1000;
	TimeLine(){
		this.setLayout(new BorderLayout());
		this.setPreferredSize(new Dimension(1000,100));
		this.timeLineSlider=new JSlider();	
		timeLineSlider.setPaintTicks(true);
		timeLineSlider.setPreferredSize(new Dimension(2000,100));
		timeLineSlider.setMajorTickSpacing(1);
		
		System.out.println(timeLineSlider.getWidth()+", "+timeLineSlider.getWidth()/132);
		timeLineSlider.setSnapToTicks(true);
		this.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 1));
		timeLineSlider.setAutoscrolls(true);
		this.setAutoscrolls(true);
		
		timeLineSlider.setUI(new SliderJumpUI());
		
		
		this.add(timeLineSlider);
	
	}
	@Override
	 public Dimension getPreferredSize() {
	        return new Dimension(variableWidth, 100);//or whatever you need-->maybe set up a separate method to calculate these values as they change so that they are ready when called.
	    }
	public void setTimeLineWidth(int i) {
		variableWidth=i;
		
	}
	
	
}
