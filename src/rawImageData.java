
public class rawImageData {

	public double gTheta=0;
	public double gMag=0;
	public edgeStrength edgeType=edgeStrength.NONE;
	
	
	public void setTheta(double in) {
		this.gTheta=in;
	}
	public void setMag(double in) {
		this.gMag=in;
	}
	
	
	public double getTheta() {
		return this.gTheta;
	}
	
	public double getMag() {
		return this.gMag;
	}
}
