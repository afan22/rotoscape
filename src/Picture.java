import java.awt.*;

import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.io.File;

import javax.imageio.ImageIO;

import org.apache.tools.ant.taskdefs.Java;


public class Picture extends SimplePicture {

	private double[][] kernelX = { { -1, 0, 1 }, { -2, 0, 2 }, { -1, 0, 1 } };

	private double[][] kernelY = { { -1, -2, -1 }, { 0, 0, 0 }, { 1, 2, 1 } };

	private rawImageData[][] processedData;

	private int maxGradient = -1;
	private int minGradient = Integer.MAX_VALUE;
	
	///////////////////// constructors //////////////////////////////////

	/**
	 * Constructor that takes no arguments
	 */
	public Picture() {
		/*
		 * not needed but use it to show students the implicit call to super() child
		 * constructors always call a parent constructor
		 */
		super();
	}

	/**
	 * Constructor that takes a file name and creates the picture
	 * 
	 * @param fileName
	 *            the name of the file to create the picture from
	 */
	public Picture(String fileName) {
		// let the parent class handle this fileName
		super(fileName);
	}

	/**
	 * Constructor that takes the width and height
	 * 
	 * @param height
	 *            the height of the desired picture
	 * @param width
	 *            the width of the desired picture
	 */
	public Picture(int height, int width) {
		// let the parent class handle this width and height
		super(width, height);
	}

	/**
	 * Constructor that takes a picture and creates a copy of that picture
	 * 
	 * @param copyPicture
	 *            the picture to copy
	 */
	public Picture(Picture copyPicture) {
		// let the parent class do the copy
		super(copyPicture);
	}

	/**
	 * Constructor that takes a buffered image
	 * 
	 * @param image
	 *            the buffered image to use
	 */
	public Picture(BufferedImage image) {
		super(image);
	}

	////////////////////// methods ///////////////////////////////////////

	/**
	 * Method to return a string with information about this picture.
	 * 
	 * @return a string with information about the picture such as fileName, height
	 *         and width.
	 */
	public String toString() {
		String output = "Picture, filename " + getFileName() + " height " + getHeight() + " width " + getWidth();
		return output;

	}

	public void sharpen(int contrast) {

		Pixel[][] pixels = this.getPixels2D();

		for (Pixel[] rowArray : pixels) {
			for (Pixel pixelObj : rowArray) {

				double f = (259 * (contrast + 255)) / (255 * (259 - contrast));

				pixelObj.setRed((int) Math.floor(f * (pixelObj.getRed() - 128) + 128));
				pixelObj.setGreen((int) Math.floor(f * (pixelObj.getGreen() - 128) + 128));
				pixelObj.setBlue((int) Math.floor(f * (pixelObj.getBlue() - 128) + 128));

			}
		}

	}

	public double distance(int width, int focal, int perwidth) {
		return (width * focal) / perwidth;
	}

	public void edgeDetection2(int edgeDist) {

		try {
			for (int row = 1; row < this.getHeight() - 2; row += 2) {
				for (int col = 1; col < this.getWidth() - 1; col++) {// +=2

					if (colorDistance(getColor(this.getBasicPixel(col, row)),
							getColor(this.getBasicPixel(col + 1, row))) > edgeDist) {
						setColor(Color.BLACK, row, col);

						// pixels[row][col+1].setColor(Color.BLACK);
					} else {
						setColor(Color.white, row, col);
						// pixels[row][col+1].setColor(Color.WHITE);
					}

					if (colorDistance(getColor(this.getBasicPixel(col, row + 1)),
							getColor(this.getBasicPixel(col + 1, row + 1))) > edgeDist) {
						setColor(Color.BLACK, row + 1, col);

						// pixels[row][col+1].setColor(Color.BLACK);
					} else {
						setColor(Color.white, row + 1, col);
						// pixels[row][col+1].setColor(Color.WHITE);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void sobel() {

		int[][] edgeColors = new int[this.getWidth()][this.getHeight()];

		for (int i = 1; i < this.getWidth() - 1; i++) {
			for (int j = 1; j < this.getHeight() - 1; j++) {

				int gx = convolute(kernelX, i, j);
				int gy = convolute(kernelY, i, j);

				double gval = Math.sqrt((gx * gx) + (gy * gy));
				int g = (int) gval;

				if (maxGradient < g) {
					maxGradient = g;
				}

				edgeColors[i][j] = g;
			}
		}
		double scale = 255.0 / maxGradient;

		for (int row = 0; row < this.getHeight(); row++) {
			for (int col = 0; col < this.getWidth(); col++) {// +=2
				edgeColors[col][row] = (int) (edgeColors[col][row] * scale);
				edgeColors[col][row] = 0xff000000 | (edgeColors[col][row] << 16) | (edgeColors[col][row] << 8)
						| edgeColors[col][row];
				this.getBufferedImage().setRGB(col, row, edgeColors[col][row]);

			}
		}

	}

	public void guassianBlur(double sigma, int size) {

	
		double[][] guassKernel = new double[size][size];
		int[][] edgeColors = new int[this.getWidth()][this.getHeight()];

		guassKernel = genGuassKernel(size, sigma);

		for (int j = 1; j < this.getHeight() - 1; j++) {
			for (int i = 1; i < this.getWidth() - 1; i++) {

				int gy = convolute(guassKernel, i, j);
				edgeColors[i][j] = gy;

			}
		}

		int pixelColor;
		for (int row = 0; row < this.getHeight(); row++) {
			for (int col = 0; col < this.getWidth(); col++) {// +=2
				int val = edgeColors[col][row];
				int pixel = (val << 16) | (val << 8) | (val);
				this.getBufferedImage().setRGB(col, row, pixel);

			}
		}

	}

	public void canny(double sigma, int kernelSize, double lowThresholdRatio, double highThresholdRatio, File fi) {
		try {
			guassianBlur(sigma, kernelSize);
			cannySobel();
			nonMaxSuppression();
			doubleThreshold(lowThresholdRatio, highThresholdRatio);
			edgeTracking();
			clean();
			processToBuffImage();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void cannySobel() {
		try {
			processedData = new rawImageData[this.getWidth()][this.getHeight()];

			for (int i = 1; i < this.getWidth() - 2; i++) {
				for (int j = 1; j < this.getHeight() - 2; j++) {

					int gx = convolute(kernelX, i, j);
					int gy = convolute(kernelY, i, j);
					processedData[i][j] = new rawImageData();
					processedData[i][j].setMag((Math.sqrt((gx * gx) + (gy * gy))));
					processedData[i][j].setTheta(Math.atan2(gy, gx) * (180 / Math.PI));

				}

			}

		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	private void nonMaxSuppression() {
		System.out.println("open " + processedData.length + " " + processedData[1].length);
		for (int i = 2; i < this.getWidth() - 3; i++) {
			for (int j = 2; j < this.getHeight() - 3; j++) {

				// System.out.println(i + " " + j);
				// 0 deg east
				if (processedData[i][j].gTheta + 180 <= 22.5 || processedData[i][j].gTheta + 180 > 337.5) {
					if (processedData[i][j].gMag > processedData[i + 1][j].gMag
							&& processedData[i][j].gMag > processedData[i - 1][j].gMag) {
						// int temp = (int) processedData[i][j].gMag;
						// temp = 0xff000000 | (temp << 16) | (temp << 8) | temp;
						// this.getBufferedImage().setRGB(i, j, temp);
					} else {
						// setColor(Color.BLACK, j,i);
						processedData[i][j].gMag = 0;
					}
				}

				// 45 deg northEast
				else if (processedData[i][j].gTheta + 180 > 22.5 && processedData[i][j].gTheta + 180 <= 67.5) {
					if (processedData[i][j].gMag > processedData[i + 1][j + 1].gMag
							&& processedData[i][j].gMag > processedData[i - 1][j - 1].gMag) {
						// int temp = (int) processedData[i][j].gMag;
						// temp = 0xff000000 | (temp << 16) | (temp << 8) | temp;
						// this.getBufferedImage().setRGB(i, j, temp);
					} else {
						// setColor(Color.BLACK, j,i);
						processedData[i][j].gMag = 0;
					}
				}

				// 90 deg north
				else if (processedData[i][j].gTheta + 180 > 67.5 && processedData[i][j].gTheta + 180 <= 112.5) {
					if (processedData[i][j].gMag > processedData[i][j + 1].gMag
							&& processedData[i][j].gMag > processedData[i][j - 1].gMag) {
						// int temp = (int) processedData[i][j].gMag;
						// temp = 0xff000000 | (temp << 16) | (temp << 8) | temp;
						// this.getBufferedImage().setRGB(i, j, temp);
					} else {
						// setColor(Color.BLACK, j,i);
						processedData[i][j].gMag = 0;
					}
				}

				// 135 deg northWest
				else if (processedData[i][j].gTheta + 180 > 112.5 && processedData[i][j].gTheta + 180 <= 157.5) {
					if (processedData[i][j].gMag > processedData[i - 1][j + 1].gMag
							&& processedData[i][j].gMag > processedData[i - 1][j + 1].gMag) {
						// int temp = (int) processedData[i][j].gMag;
						// temp = 0xff000000 | (temp << 16) | (temp << 8) | temp;
						// this.getBufferedImage().setRGB(i, j, temp);
					} else {
						// setColor(Color.BLACK, j,i);
						processedData[i][j].gMag = 0;
					}
				}

				// 180 deg west
				else if (processedData[i][j].gTheta + 180 > 157.5 && processedData[i][j].gTheta + 180 <= 202.5) {
					if (processedData[i][j].gMag > processedData[i - 1][j].gMag
							&& processedData[i][j].gMag > processedData[i + 1][j].gMag) {
						// int temp = (int) processedData[i][j].gMag;
						// temp = 0xff000000 | (temp << 16) | (temp << 8) | temp;
						// this.getBufferedImage().setRGB(i, j, temp);
					} else {
						// setColor(Color.BLACK, j,i);
						processedData[i][j].gMag = 0;
					}
				}

				// 225 deg southWest
				else if (processedData[i][j].gTheta + 180 > 202.5 && processedData[i][j].gTheta + 180 <= 247.5) {
					if (processedData[i][j].gMag > processedData[i - 1][j - 1].gMag
							&& processedData[i][j].gMag > processedData[i + 1][j + 1].gMag) {
						// int temp = (int) processedData[i][j].gMag;
						// temp = 0xff000000 | (temp << 16) | (temp << 8) | temp;
						// this.getBufferedImage().setRGB(i, j, temp);
					} else {
						// setColor(Color.BLACK, j,i);
						processedData[i][j].gMag = 0;
					}
				}

				// 270 deg south
				else if (processedData[i][j].gTheta + 180 > 247.5 && processedData[i][j].gTheta + 180 <= 292.5) {
					if (processedData[i][j].gMag > processedData[i][j - 1].gMag
							&& processedData[i][j].gMag > processedData[i][j + 1].gMag) {
						// int temp = (int) processedData[i][j].gMag;
						// temp = 0xff000000 | (temp << 16) | (temp << 8) | temp;
						// this.getBufferedImage().setRGB(i, j, temp);
					} else {
						// setColor(Color.BLACK, j,i);
						processedData[i][j].gMag = 0;
					}
				}

				// 315 deg southEast
				else if (processedData[i][j].gTheta + 180 > 292.5 && processedData[i][j].gTheta + 180 <= 337.5) {
					if (processedData[i][j].gMag > processedData[i + 1][j - 1].gMag
							&& processedData[i][j].gMag > processedData[i - 1][j + 1].gMag) {
						// int temp = (int) processedData[i][j].gMag;
						// temp = 0xff000000 | (temp << 16) | (temp << 8) | temp;
						// this.getBufferedImage().setRGB(i, j, temp);
					} else {
						// setColor(Color.BLACK, j,i);
						processedData[i][j].gMag = 0;
					}
				}
			}

		}

	}

	

	private void doubleThreshold(double lowThreshold, double highThreshold) {
		//double highThreshold = maxGradient * highThresholdRatio;
		//double lowThreshold = highThreshold * lowThresholdRatio;
		System.out.println("threshold: "+lowThreshold+", "+highThreshold+", "+maxGradient+", "+minGradient);
		double highThresholdRatio=otsuTreshold(this.getBufferedImage());
			double lowThresholdRatio=0.5*highThresholdRatio;
		double norm=0;
		
		for (int i = 1; i < this.getWidth() - 2; i++) {
			for (int j = 1; j < this.getHeight() - 2; j++) {
				norm=processedData[i][j].getMag();
				//norm=(processedData[i][j].getMag()-av)/(std);
				//norm=((processedData[i][j].getMag() -minGradient) 
			        //    / (maxGradient - minGradient))
			          //  * (highThresholdRatio - lowThresholdRatio) + lowThresholdRatio;
				System.out.println(norm);
				if (norm > highThresholdRatio) {
					processedData[i][j].edgeType = edgeStrength.STRONG;
				} else if (norm < highThresholdRatio
						&& norm > lowThresholdRatio) {
					processedData[i][j].edgeType = edgeStrength.WEAK;
				}
				if (norm < lowThresholdRatio) {
					processedData[i][j].edgeType = edgeStrength.NONE;

				}
			}
		}
		System.out.println("threshold: "+lowThresholdRatio+", "+highThresholdRatio+", "+maxGradient+", "+minGradient);
	}

	private void edgeTracking() {

		for (int i = 2; i < this.getWidth() - 3; i++) {
			for (int j = 2; j < this.getHeight() - 3; j++) {
				switch (processedData[i][j].edgeType) {

				case NONE:
					processedData[i][j].setMag(0);
					break;
				case STRONG:
					break;
				case WEAK:
					if (!isTrueStrongEdge(i, j)) {
						processedData[i][j].setMag(0);
					} else {
						processedData[i][j].edgeType = edgeStrength.STRONG;
					}
					break;

				}

			}
		}
	}

	private void clean() {
		for (int i = 1; i < this.getWidth() - 2; i++) {
			for (int j = 1; j < this.getHeight() - 2; j++) {
				if (processedData[i][j].edgeType == edgeStrength.WEAK) {
					processedData[i][j].setMag(0);
				}
				if (processedData[i][j].edgeType == edgeStrength.STRONG) {
					processedData[i][j].setMag(255);
				}
			}
		}
	}

	private void processToBuffImage() {
		for (int row = 1; row < this.getHeight() - 2; row++) {
			for (int col = 1; col < this.getWidth() - 2; col++) {// +=2
				int temp = (int) processedData[col][row].gMag;
				temp = 0xff000000 | (temp << 16) | (temp << 8) | temp;
				this.getBufferedImage().setRGB(col, row, temp);

			}
		}
	}

	private double[][] genGuassKernel(int size, double sigma) {
		double[][] filter = new double[size][size];
		int bounds = ((size - 1) / 2);
		double sum = 0.0;
		double r, s = 2.0 * sigma * sigma;
		for (int row = -bounds; row <= bounds; row++) {
			for (int col = -bounds; col <= bounds; col++) {
				r = Math.sqrt(col * col + row * row);
				filter[row + bounds][col + bounds] = ((Math.exp(-(r * r) / s)) / (Math.PI * s));
				sum += filter[row + bounds][col + bounds];
			}
		}

		// normalization

		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				filter[i][j] /= sum;
				// System.out.print(filter[i][j] + ",");
			}
			System.out.print("\n");
		}
		return filter;
	}





	/**
	 * Method to update the picture based on the passed color values for this pixel
	 * 
	 * @param alpha
	 *            the alpha (transparency) at this pixel
	 * @param red
	 *            the red value for the color at this pixel
	 * @param green
	 *            the green value for the color at this pixel
	 * @param blue
	 *            the blue value for the color at this pixel
	 */

	private static double colorDistance(Color color1, Color color2) {
		double redDistance = color1.getRed() - color2.getRed();
		double greenDistance = color1.getGreen() - color2.getGreen();
		double blueDistance = color1.getBlue() - color2.getBlue();
		double distance = Math
				.sqrt(redDistance * redDistance + greenDistance * greenDistance + blueDistance * blueDistance);
		return distance;
	}

	private Color getColor(int value) {
		/*
		 * get the value at the location from the picture as a 32 bit int with alpha,
		 * red, green, blue each taking 8 bits from left to right
		 */

		// get the red value (starts at 17 so shift right 16)
		// then AND it with all 1's for the first 8 bits to
		// end up with a resulting value from 0 to 255
		int red = (value >> 16) & 0xff;

		// get the green value (starts at 9 so shift right 8)
		int green = (value >> 8) & 0xff;

		// get the blue value (starts at 0 so no shift required)
		int blue = value & 0xff;

		return new Color(red, green, blue);
	}

	private int Greyscale(int rgb) {
		int r = (rgb >> 16) & 0xff;
		int g = (rgb >> 8) & 0xff;
		int b = (rgb) & 0xff;

		// from https://en.wikipedia.org/wiki/Grayscale, calculating luminance
		int gray = (int) (0.2126 * r + 0.7152 * g + 0.0722 * b);
		// int gray = (r + g + b) / 3;

		return gray;
	}

	private int convolute(double[][] kernel, int x, int y) {
		int sum = 0;
		int xn, yn;
		for (int row = 0; row < kernel.length; row++) {
			for (int col = 0; col < kernel[row].length; col++) {
			
				xn = x - kernel.length / 2 + row;
				yn = y - kernel[0].length / 2 + col;
				xn = constrain(xn, 0, this.getWidth() - 1);
				yn = constrain(yn, 0, this.getHeight() - 1);
				sum += (Greyscale(this.getBufferedImage().getRGB(xn, yn)) * kernel[col][row]);
				
			}
		}
		return sum;

	}

	private boolean isTrueStrongEdge(int i, int j) {

		for (int x = -1; x < 2; x++) {
			for (int y = -1; y < 2; y++) {
				// System.out.println("xyij: "+x+", "+y+", "+i+", "+j);
				if (processedData[i + x][j + y].edgeType == edgeStrength.STRONG) {
					return true;
				}
			}
		}
		return false;

	}

	public int constrain(int x, int a, int b) {
		if (x < a) {
			return a;
		} else if (b < x) {
			return b;
		} else {
			return x;
		}
	}
	
	
	
}
