import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.FileImageOutputStream;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.ImageOutputStream;

/**
 * This class contains class (static) methods that will help you test the
 * Picture class methods. Uncomment the methods and the code in the main to
 * test.
 * 
 * @author Barbara Ericson
 */
public class PictureTester {
	/** Method to test zeroBlue */
	ArrayList<Picture> buff = new ArrayList<Picture>();
	static ArrayList<BufferedImage> buf;
	static File input;
	static MessageConsole mc;


	
	public static void testg(int ed) throws IOException {
		
		ImageReader reader = ImageIO.getImageReadersByFormatName("gif").next();
		
		ImageInputStream stream = ImageIO.createImageInputStream(input);
		reader.setInput(stream);
		buf = gifread2.readGIF(reader);
		// buf = gifreader.getFrames();
		// fill arraylist with changed images
		for (int i = 0; i < buf.size(); i++) {
			Picture san = null;
			san = new Picture(buf.get(i));
			san.sharpen(50);
			san.edgeDetection2(ed);

			buf.set(i, san.getBufferedImage());
			// san.explore();

			buf.set(i, conv.convertRGBAToIndexed(buf.get(i)));

		}

	}

	public static void contrast() {
		Picture swan = new Picture(FileChooser.pickAFile());
		swan.sharpen(100);
		swan.explore();
	}

	public static void togif() throws Exception {
		if (buf.size() > 1) {
			// grab the output image type from the first image in the sequence
			BufferedImage firstImage = buf.get(0);

			// create a new BufferedOutputStream with the last argument
			String fileName = input.getPath();
			fileName = fileName.substring(0, fileName.lastIndexOf("."));
			File fi = new File(fileName + "Roto2.gif");

			ImageOutputStream output = new FileImageOutputStream(fi);

			// create a gif sequence with the type of the first image, 1 second
			// between frames, which loops continuously
			GifSequenceWriter writer = new GifSequenceWriter(output, firstImage.getType(), 1, true);

			// write out the first image to our sequence...
			writer.writeToSequence(firstImage);
			for (int i = 1; i < buf.size() - 1; i++) {
				BufferedImage nextImage = ((buf.get(i)));
				writer.writeToSequence(nextImage);
			}

			writer.close();
			output.close();
		} else {
			System.out.println("Usage: java GifSequenceWriter [list of gif files] [output file]");
		}
	}

	/**
	 * Main method for testing. Every class can have a main method in Java
	 * @param in 
	 * 
	 * @throws Exception
	 */
	public PictureTester(int edge, File in,MessageConsole mc) throws Exception {
		try{
			this.mc=mc;
			input=in;
		testg(edge);
		togif();
		System.out.println("Finished compiling gif");
		}
		catch(Exception e){
			System.out.println("error:input Valid file");
			
		}

	}
}