import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import javax.imageio.ImageIO;
import javax.swing.SwingWorker;

public class rotoscopeUtil  implements Runnable{

	protected File input;
	protected MessageConsole mc;
	protected int edge,sharp,kernelSize;
	protected double sigma;
	protected String detectionAlgorithm="Open";
	protected BufferedImage[] outputImage=null;
	protected imagePreviewPanel previewPanel;
	public ArrayList<BufferedImage>outbuf=new ArrayList<BufferedImage>();
	
	
	rotoscopeUtil( File in, MessageConsole mc,imagePreviewPanel previewPanel){
		try {
			this.mc = mc;
			this.previewPanel=previewPanel;
			input = in;
			
			
		
		} catch (java.lang.Exception e) {
			mc.ConsolePrintLn("error:input Valid file");
			e.printStackTrace();

		}

	}
	
	
	
	
	public void setOperationSimple(int edgeDistance){
		this.detectionAlgorithm="Simple";
		this.edge=edgeDistance;
	}

	public void setOperationSobel(double sigma,int kernelSize) {
		this.detectionAlgorithm="Sobel";
		this.sigma=sigma;
		this.kernelSize=kernelSize;
	}
	
	public void setOperationCanny(double sigma,int kernelSize) {
		this.detectionAlgorithm="Canny";
		this.sigma=sigma;
		this.kernelSize=kernelSize;
	}
	
	/**
	 * Just woke up like this. sets operation to return an unedited image.
	 */
	public void setOperationNoFilter() {
		this.detectionAlgorithm="NoFilter";
		
	}
	





	/*@Override
	protected BufferedImage[] doInBackground() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	protected void process(List<BufferedImage> chunks) {
		
	}
	
	@Override 
	protected void done() {
		
	}*/




	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}
	
	
}
