import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.plaf.FileChooserUI;

import java.io.*;
import java.net.*;

public class FileChooser {

	/////////////////////// methods /////////////////////////////

	/**
	 * Method to get the full path for the passed file name
	 * 
	 * @param fileName
	 *            the name of a file
	 * @return the full path for the file
	 */
	public static String getMediaPath(String fileName) {
		String path = null;
		String directory = getMediaDirectory();
		// get the full path
		path = directory + fileName;
		return path;
	}

	/**
	 * Method to pick an item using the file chooser
	 * 
	 * @param fileChooser
	 *            the file Chooser to use
	 * @return the path name
	 */
	public static String pickPath(JFileChooser fileChooser) {
		String path = null;

		/*
		 * create a JFrame to be the parent of the file chooser open dialog if you don't
		 * do this then you may not see the dialog.
		 */

		JFrame frame = new JFrame();
		frame.setAlwaysOnTop(true);

		// get the return value from choosing a file
		int returnVal = fileChooser.showOpenDialog(frame);

		fileChooser.updateUI();
		// if the return value says the user picked a file
		if (returnVal == JFileChooser.APPROVE_OPTION)
			path = fileChooser.getSelectedFile().getPath();
		return path;
	}

	/**
	 * Method to let the user pick a file and return the full file name as a string.
	 * If the user didn't pick a file then the file name will be null.
	 * 
	 * @return the full file name of the picked file or null
	 */
	public static String pickAFile() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		JFileChooser fileChooser = null;

		// start off the file name as null
		String fileName = null;

		// get the current media directory
		String mediaDir = getMediaDirectory();

		/*
		 * create a file for this and check that the directory exists and if it does set
		 * the file chooser to use it
		 */
		try {
			File file = new File(mediaDir);
			if (file.exists())
				fileChooser = new JFileChooser(file);
		} catch (Exception ex) {
		}

		// if no file chooser yet create one
		if (fileChooser == null) {
			fileChooser = new JFileChooser();

			// Only allow accepted file types
			fileChooser.setAcceptAllFileFilterUsed(false);
			fileChooser.addChoosableFileFilter(
					new FileNameExtensionFilter("RotoScape", "mp4", "mov", "png", "jpg", "gif"));
			fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Video", "mp4", "mov"));
			fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Images", "png", "jpg"));
			fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Gif", "gif"));
		}

		// pick the file
		fileName = pickPath(fileChooser);

		return fileName;
	}

	// TODO jdoc

	public static String pickSaveFile(File in) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		JFileChooser fileChooser = null;

		// start off the file name as null
		String fileName = null;

		// get the current media directory
		String mediaDir = in.getPath();

		/*
		 * create a file for this and check that the directory exists and if it does set
		 * the file chooser to use it
		 */
		try {
			
			File file = new File(mediaDir);
			String extention=in.toString().substring(in.toString().lastIndexOf(".") + 1,in.toString().length());
			
			if (file.exists()) {
				fileChooser = new JFileChooser(file);
				fileChooser.setAcceptAllFileFilterUsed(false);
				fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Save",extention));
			}

		} catch (Exception ex) {
		}

		// if no file chooser yet create one
		if (fileChooser == null) {
			fileChooser = new JFileChooser();

			fileChooser.setSelectedFile(in);

			// Only allow accepted file types
			fileChooser.setAcceptAllFileFilterUsed(false);
			fileChooser.addChoosableFileFilter(
					new FileNameExtensionFilter("RotoScape", "mp4", "mov", "png", "jpg", "gif"));
			fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Video", "mp4", "mov"));
			fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Images", "png", "jpg"));
			fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Gif", "gif"));
		}
		fileChooser.setSelectedFile(in);
		// pick the file
		fileName = pickSavePath(fileChooser);

		return fileName;
	}

	// TODO javadoc
	public static String pickSavePath(JFileChooser fileChooser) {
		String path = null;

		/*
		 * create a JFrame to be the parent of the file chooser open dialog if you don't
		 * do this then you may not see the dialog.
		 */

		JFrame frame = new JFrame();
		frame.setAlwaysOnTop(true);

		// get the return value from choosing a file
		int returnVal = fileChooser.showSaveDialog(frame);

		fileChooser.updateUI();
		// if the return value says the user picked a file
		if (returnVal == JFileChooser.APPROVE_OPTION)
			path = fileChooser.getSelectedFile().getPath();
		return path;
	}

	/**
	 * Method to get the directory for the media
	 * 
	 * @return the media directory
	 */
	public static String getMediaDirectory() {
		String directory = null;
		File dirFile = null;

		// try to find the images directory
		try {
			// get the URL for where we loaded this class
			Class<?> currClass = Class.forName("FileChooser");
			URL classURL = currClass.getResource("FileChooser.class");
			URL fileURL = new URL(classURL, "../images/");
			directory = fileURL.getPath();
			directory = URLDecoder.decode(directory, "UTF-8");
			dirFile = new File(directory);
			if (dirFile.exists()) {
				// setMediaPath(directory);
				return directory;
			}
		} catch (Exception ex) {
		}

		return directory;
	}

}
