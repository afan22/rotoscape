import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;

import javax.swing.JMenuBar;

public class RoundMenuBar extends JMenuBar {
	 public final static Color BUTTON_TOP_GRADIENT = new Color(235, 235, 245);
	  public final static Color BUTTON_BOTTOM_GRADIENT = new Color(11, 230, 37);
	public RoundMenuBar() {}
		  @Override
		  protected void paintComponent(Graphics g) {
		    Graphics2D g2 = (Graphics2D)g.create();
		    
		    
		   
            
		    RenderingHints qualityHints =
		      new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		    qualityHints.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		    g2.setRenderingHints(qualityHints);

		    g2.setPaint(new GradientPaint(new Point(0, 0), BUTTON_TOP_GRADIENT, new Point(0, getHeight()),
		                                  BUTTON_BOTTOM_GRADIENT));
		    //g2.fillRoundRect(0, 0, getWidth(), getHeight(), 20, 40);
		    
		    g2.fill(new RightEnd(getWidth(),getHeight(),25));
		    
		    AffineTransform tx = AffineTransform
                    .getScaleInstance(-1, 1);
            tx.translate(-getWidth(), 0);
            g2.transform(tx);
            g2.fill(new RightEnd(getWidth(),getHeight(),25));
            
         
		    g2.dispose();
		  
	}
		  public class RightEnd extends Path2D.Float {

		        public RightEnd(float width, float height, float radius) {
		            moveTo(width/2, 0);
		            lineTo(width , 0);
		            lineTo(width, height - radius);
		            curveTo(width, height, width, height, width - radius, height);
		            lineTo(width/2, height);
		            closePath();
		        }



		    }
}
