import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.RoundRectangle2D;

import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.plaf.metal.MetalSliderUI;

/**
 * remember to update this DipSh*t otherwise thats plagerism
 * 
 * @author aterai
 *
 */
public class SliderJumpUI extends MetalSliderUI {
	@Override
	protected TrackListener createTrackListener(JSlider slider) {
		return new TrackListener() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (UIManager.getBoolean("Slider.onlyLeftMouseButtonDrag") && SwingUtilities.isLeftMouseButton(e)) {
					JSlider slider = (JSlider) e.getComponent();
					switch (slider.getOrientation()) {
					case SwingConstants.VERTICAL:
						slider.setValue(valueForYPosition(e.getY()));
						break;
					case SwingConstants.HORIZONTAL:
						slider.setValue(valueForXPosition(e.getX()));
						break;
					default:
						throw new IllegalArgumentException("orientation must be one of: VERTICAL, HORIZONTAL");
					}
					super.mousePressed(e); // isDragging = true;
					super.mouseDragged(e);

				} else {
					super.mousePressed(e);

				}
			}

			@Override
			public boolean shouldScroll(int direction) {
				return false;
			}
		};
	}

	@Override
	public void paintTrack(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		Stroke old = g2d.getStroke();

		/*
		 * Rectangle trackBounds = trackRect; if (slider.getOrientation() ==
		 * SwingConstants.HORIZONTAL) { g2d.setColor(Color.darkGray);
		 * g2d.drawLine(trackRect.x, trackRect.y + trackRect.height / 2, trackRect.x +
		 * trackRect.width, trackRect.y + trackRect.height / 2); int lowerX =
		 * thumbRect.width / 2; int upperX = thumbRect.x + (thumbRect.width / 2); int cy
		 * = (trackBounds.height / 2) - 2; g2d.translate(trackBounds.x, trackBounds.y +
		 * cy);
		 * 
		 * g2d.drawLine(lowerX - trackBounds.x, 2, upperX - trackBounds.x, 2);
		 * g2d.translate(-trackBounds.x, -(trackBounds.y + cy));
		 * 
		 * }
		 */

	}

	@Override
	protected void paintMajorTickForHorizSlider(Graphics g, Rectangle tickBounds, int x) {
		int y = tickRect.height / 4;
		Color saved = g.getColor();
		g.setColor(Color.LIGHT_GRAY);
		
		/*g.drawLine(x, y-50,x , y );
		g.drawLine(x, y-50, x+tickBounds.width, y-50);
		g.drawLine(x, y, x+tickBounds.width, y);*/
		
		g.fillRect(x, y-50,tickRect.width,y+20);
		g.setColor(Color.DARK_GRAY);
		g.drawRect(x, y-50,tickRect.width,y+20);
		
		
		g.setColor(saved);
	}

	@Override
	public void paintThumb(Graphics g) {
		Rectangle knobBounds = thumbRect;

		int w = knobBounds.width;
		int h = knobBounds.height;
		Graphics2D g2d = (Graphics2D) g.create();
		Shape thumbShape = createThumbShape(w - 1, h - 1);
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.translate(knobBounds.x, knobBounds.y);
		System.out.println(knobBounds.y);
		g2d.setColor(Color.CYAN);
		g2d.fill(thumbShape);

		g2d.dispose();
	}

	@Override
	protected void calculateThumbSize() {
		super.calculateThumbSize();

		thumbRect.setSize(thumbRect.width, thumbRect.height);

	}

	@Override
	public int getTickLength()
	   {
		tickRect.y=60;
	    return 80;
	   }
	@Override
	protected Dimension getThumbSize() {
		return new Dimension(12, 70);
	}

	private Shape createThumbShape(int width, int height) {
		RoundRectangle2D shape = new RoundRectangle2D.Double(5, 5, 3, 60, 0, 0);

		return shape;
	}
}
