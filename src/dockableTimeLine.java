import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;

import com.javadocking.dockable.DraggableContent;
import com.javadocking.drag.DragListener;

public class dockableTimeLine extends JPanel implements DraggableContent {

	
	protected int frameArraySize=0;
	protected int selectedFrame=0;
	protected imagePreviewPanel linkedPreviewPanel;
	
	private JButton nextFrame,prevFrame;
	private JLabel frameCountLbl;
	private TimeLine timeLineSlider;
	private JScrollPane scroller;
	
	dockableTimeLine(imagePreviewPanel linkedPreviewPanel){
		super();
		
		this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		
		this.linkedPreviewPanel=linkedPreviewPanel;
		
		JPanel frameChooser=new JPanel();
		frameChooser.setLayout(new BoxLayout(frameChooser,BoxLayout.X_AXIS));
		this.frameCountLbl=new JLabel("0/0",SwingConstants.CENTER);
		this.nextFrame=new JButton(">");
		this.prevFrame=new JButton("<");
		
		frameCountLbl.setMinimumSize(new Dimension(50,30));
		frameCountLbl.setPreferredSize(new Dimension(60,30));
		frameCountLbl.setMaximumSize(new Dimension(70,30));
		frameCountLbl.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		frameChooser.setBorder(BorderFactory.createLineBorder(Color.darkGray));
		
		frameChooser.add(prevFrame);
		frameChooser.add(frameCountLbl,BorderLayout.CENTER);
		frameChooser.add(nextFrame);
		

		timeLineSlider=new TimeLine();
		scroller=new JScrollPane(timeLineSlider);
		
		scroller.setPreferredSize(new Dimension(500,500));
		scroller.setMaximumSize(new Dimension(500,500));
		scroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scroller.setAutoscrolls(true);
		
		
		this.add(frameChooser);
		this.add(Box.createVerticalStrut(20));
		
		this.add(scroller,BorderLayout.SOUTH);
		this.add(Box.createVerticalStrut(10));
		
		prevFrame.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(selectedFrame>0)
				{
					selectedFrame-=1;
					linkedPreviewPanel.setSelectedImage(selectedFrame);
					frameCountLbl.setText(selectedFrame+"/"+frameArraySize);
				}
			}
		});
		
		nextFrame.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(selectedFrame<frameArraySize-1)
				{
					selectedFrame+=1;
					linkedPreviewPanel.setSelectedImage(selectedFrame);
					frameCountLbl.setText((selectedFrame+1)+"/"+frameArraySize);
				}
			}
		});
		
		}
	
	
	
	public void setFrameArraySize(int size) {
		this.frameArraySize=size;
		frameCountLbl.setText(selectedFrame+"/"+frameArraySize);
		timeLineSlider.setTimeLineWidth((frameArraySize*12)+20);
		timeLineSlider.revalidate();
	}
	
	
	

	@Override
	public void addDragListener(DragListener dragListener) {
		addMouseListener(dragListener);
		addMouseMotionListener(dragListener);
		
	}
	
	
	
	
	
	
	
	
		
	
}
