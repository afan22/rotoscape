import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.ButtonUI;
import javax.swing.text.DefaultCaret;
import javax.swing.text.JTextComponent;

import com.google.common.io.Files;
import com.javadocking.DockingManager;
import com.javadocking.dock.LineDock;
import com.javadocking.dock.Position;
import com.javadocking.dock.SplitDock;
import com.javadocking.dock.TabDock;
import com.javadocking.dockable.DefaultDockable;
import com.javadocking.dockable.Dockable;
import com.javadocking.dockable.DockingMode;
import com.javadocking.model.FloatDockModel;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

@SuppressWarnings("unused")
public class RotoScape extends JPanel {
	JFrame frame;
	MessageConsole mc;
	JTextField edgeDistanceTxt, kernelCannyTxt, sigmaCannyTxt, kernelSobelTxt, sigmaSobelTxt;
	JTextField sharpenTxt;
	JMenuBar inputSourceMnu;
	JMenu edgeAlgorithmMnu, fileMnu;
	JMenuItem outputMni, openMni, saveMni;
	JRadioButtonMenuItem sobelRad, simpleRad, cannyRad;
	JButton runBtn;
	ButtonGroup algorithmTypeGroup;
	File in, out;
	imagePreviewPanel previewPanel;

	public rotoscopeUtil imageProcess;

	public static final int FRAME_WIDTH = 600;
	public static final int FRAME_HEIGHT = 450;

	RotoScape(JFrame frame) {
		super(new BorderLayout());

		this.frame = frame;
		this.edgeDistanceTxt = new JTextField("Edge distance");
		this.kernelCannyTxt = new JTextField("Guass Kernel Size");
		this.sigmaCannyTxt = new JTextField("Sigma");
		this.kernelSobelTxt = new JTextField("Kernel");
		this.sigmaSobelTxt = new JTextField("Sigma");
		this.runBtn = new JButton("run");
		this.mc = new MessageConsole();
		this.previewPanel = new imagePreviewPanel();

		frame.getRootPane().setBorder(BorderFactory.createRaisedSoftBevelBorder());

		FloatDockModel dockModel = new FloatDockModel();
		dockModel.addOwner("frame0", frame);
		// Give the dock model to the docking manager.
		DockingManager.setDockModel(dockModel);

		dockableScrollPane ds = new dockableScrollPane("hi", mc);
		dockableSimpleInput di = new dockableSimpleInput(this, mc, edgeDistanceTxt, sigmaCannyTxt, kernelCannyTxt,sigmaSobelTxt, kernelSobelTxt);
		dockableTimeLine dt=new dockableTimeLine(previewPanel);
		
		previewPanel.setLinkedTimeLine(dt);
		
		Dockable dockable1 = new DefaultDockable("Console", ds, "Console", null, DockingMode.ALL);
		Dockable dockable2 = new DefaultDockable("SimpleImageSettings", di, "Window 2", null, DockingMode.ALL);
		Dockable dockable3 = new DefaultDockable("TimeLine",dt,"TimeLine",null,DockingMode.ALL);

		TabDock bottomLineDock = new TabDock();
		LineDock leftLineDock = new LineDock();

		bottomLineDock.addDockable(dockable3,new Position(0));
		bottomLineDock.addDockable(dockable1, new Position(1));
		bottomLineDock.setSelectedDockable(dockable3);
		
		leftLineDock.addDockable(dockable2, new Position(0));

		leftLineDock.setMinimumSize(new Dimension(190, 0));

		SplitDock bottomSplitDock = new SplitDock();
		SplitDock leftSplitDock = new SplitDock();

		bottomSplitDock.addChildDock(bottomLineDock, new Position(Position.CENTER));
		leftSplitDock.addChildDock(leftLineDock, new Position(Position.CENTER));

		dockModel.addRootDock("leftdock", leftSplitDock, frame);
		dockModel.addRootDock("bottomdock", bottomSplitDock, frame);

		JSplitPane leftSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);

		leftSplitPane.setDividerLocation(500);
		JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		splitPane.setDividerLocation(300);

		// Add the root docks to the split panes.
		leftSplitPane.setTopComponent(this.previewPanel);
		leftSplitPane.setBottomComponent(bottomSplitDock);
		splitPane.setLeftComponent(leftSplitDock);
		splitPane.setRightComponent(leftSplitPane);

		// Add the split pane to the panel.
		add(splitPane, BorderLayout.CENTER);

		/*
		 * Dropdown Menu =====================================================
		 */
		inputSourceMnu = new RoundMenuBar();
		inputSourceMnu.setBorder(new EmptyBorder(0, 20, 2, 0));
		// inputSourceMnu.add(Box.createHorizontalStrut(25));
		inputSourceMnu.setPreferredSize(new Dimension(frame.getWidth(), 25));

		fileMnu = new JMenu("File");
		// fileMnu .setBackground(Color.gray);
		// fileMnu .setOpaque(true);
		fileMnu.setMargin(new Insets(0, 10, 0, 10));

		fileMnu.addSeparator();

		openMni = new JMenuItem("open");
		saveMni = new JMenuItem("save");

		fileMnu.add(openMni);
		fileMnu.add(saveMni);

		edgeAlgorithmMnu = new JMenu("Method");
		// edgeAlgorithmMnu.setBackground(Color.gray);
		// edgeAlgorithmMnu.setOpaque(true);
		edgeAlgorithmMnu.setMargin(new Insets(0, 10, 0, 10));
		;
		edgeAlgorithmMnu.addSeparator();

		algorithmTypeGroup = new ButtonGroup();

		sobelRad = new JRadioButtonMenuItem("Sobel");
		sobelRad.setActionCommand("Sobel");

		simpleRad = new JRadioButtonMenuItem("Simple");
		simpleRad.setSelected(true);
		simpleRad.setActionCommand("Simple");

		cannyRad = new JRadioButtonMenuItem("Canny");
		cannyRad.setActionCommand("Canny");

		algorithmTypeGroup.add(sobelRad);
		algorithmTypeGroup.add(simpleRad);
		algorithmTypeGroup.add(cannyRad);

		edgeAlgorithmMnu.add(sobelRad);
		edgeAlgorithmMnu.add(simpleRad);
		edgeAlgorithmMnu.add(cannyRad);

		inputSourceMnu.add(fileMnu);
		inputSourceMnu.add(edgeAlgorithmMnu);

		frame.add(inputSourceMnu);
		frame.setJMenuBar(inputSourceMnu);

		// Text Fields//
		// ============================================================================

		/*
		 * Buttons ======================================================
		 */

		/*
		 * runBtn.setSize(80, 50); runBtn.setLocation(300, 200);
		 * runBtn.setEnabled(false); runBtn.setVisible(true);
		 * frame.getContentPane().add(runBtn);
		 */

		// frame.setAutoRequestFocus(true);
		// frame.setSize(400, 400);
		// frame.setLayout(null);
		frame.setDefaultLookAndFeelDecorated(false);
		// frame.setVisible(true);

		// win.add(comp)

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		simpleRad.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					String detectionAlgorithm = algorithmTypeGroup.getSelection().getActionCommand();
					if (detectionAlgorithm == "Simple") {
						di.setPage(detectionAlgorithm);
					}
				} catch (Exception e1) {

				}
			}
		});

		sobelRad.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					String detectionAlgorithm = algorithmTypeGroup.getSelection().getActionCommand();
					if (detectionAlgorithm == "Sobel") {
						di.setPage(detectionAlgorithm);
					}
				} catch (Exception e1) {

				}
			}
		});

		cannyRad.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					String detectionAlgorithm = algorithmTypeGroup.getSelection().getActionCommand();
					if (detectionAlgorithm == "Canny") {
						di.setPage(detectionAlgorithm);
					}
				} catch (Exception e1) {

				}
			}
		});

		openMni.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					
					Thread process;
					in = new File(FileChooser.pickAFile());
					runBtn.setEnabled(true);
					String extention = in.toString().substring(in.toString().lastIndexOf(".") + 1,
							in.toString().length());
					switch (extention) {

					case "png":

						imageProcess = new ImgRotoscoper2(in, mc,previewPanel);
						imageProcess.detectionAlgorithm="Open";
						process=new Thread(imageProcess);
						process.start();
						break;

					case "mp4":
						imageProcess = new Mp4Rotoscoper(in, mc,previewPanel);
						imageProcess.detectionAlgorithm="Open";
						//ExecutorService executor = Executors.newSingleThreadExecutor();
						 process=new Thread(imageProcess);
						process.start();
						//Future<BufferedImage[]> future = executor.submit(imageProcess);
						//imageProcess.execute();
						//previewPanel.setImage(imageProcess.get()[0]);
						//previewPanel.repaint();
						
						// TODO write initial preview;
						break;
					}

				} catch (Exception e1) {
					e1.printStackTrace();
				}

			}
		});

		saveMni.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {

					String extension = in.toString().substring(in.toString().lastIndexOf(".") + 1,
							in.toString().length());

					out = new File(FileChooser.pickSaveFile(in));

					switch (extension) {

					case "png":

						try {

							ExecutorService executor = Executors.newSingleThreadExecutor();
							ImgRotoscoper2 process;

							// TODO process = new ImgRotoscoper2(in, mc, detectionAlgorithm,
							// sigma,kernelSize);
							// Future<BufferedImage> future = executor.submit(process);

							// ImageIO.write(future.get(),"png",out);
						} catch (Exception e1) {

						}

						break;

					case "mp4":

						break;

					}

				} catch (Exception e1) {

				}

			}
		});
		
		
		

		frame.pack();
		
	}

	public static void createAndShowGUI() {

		// Create the frame.
		JFrame frame = new JFrame("RotoScape");
		frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		// Create the panel and add it to the frame.
		RotoScape panel = new RotoScape(frame);
		frame.getContentPane().add(panel);

		// Set the frame properties and show it.
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setLocation((screenSize.width - FRAME_WIDTH) / 2, (screenSize.height - FRAME_HEIGHT) / 2);
		frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		frame.setVisible(true);

	}

	/*
	 * public void renderSimple(int edgeDistanceValue) {
	 * 
	 * 
	 * try {
	 * 
	 * 
	 * String detectionAlgorithm =
	 * algorithmTypeGroup.getSelection().getActionCommand();
	 * 
	 * String extention = in.toString().substring(in.toString().lastIndexOf(".") +
	 * 1, in.toString().length());
	 * 
	 * switch (extention) {
	 * 
	 * case "gif": try { PictureTester go = new PictureTester(edgeDistanceValue, in,
	 * mc); } catch (Exception e1) { e1.printStackTrace(); } break;
	 * 
	 * case "jpg": case "png": try { ImgRotoscoper2 go = new ImgRotoscoper2(in, mc,
	 * detectionAlgorithm, edgeDistanceValue); new Thread(go).start(); } catch
	 * (Exception e1) { e1.printStackTrace(); } break;
	 * 
	 * case "mov": case "mp4": try { Mp4Rotoscoper go = new Mp4Rotoscoper( in, mc,
	 * detectionAlgorithm, out,edgeDistanceValue); new Thread(go).start(); } catch
	 * (Exception e1) { e1.printStackTrace(); } break;
	 * 
	 * default: mc.ConsoleError("Error: input valid file");
	 * 
	 * System.out.println(extention); } } catch (Exception e1) {
	 * mc.ConsoleError("Error: fill in all required feilds"); }
	 * 
	 * }
	 * 
	 * 
	 * 
	 * public void renderSobel(double sigmaValue,int kernelSizeValue) {
	 * 
	 * try {
	 * 
	 * 
	 * String detectionAlgorithm =
	 * algorithmTypeGroup.getSelection().getActionCommand();
	 * 
	 * 
	 * String extention = in.toString().substring(in.toString().lastIndexOf(".") +
	 * 1, in.toString().length());
	 * 
	 * switch (extention) {
	 * 
	 * case "gif": try { //PictureTester go = new PictureTester(edgeDistanceValue,
	 * in, mc); } catch (Exception e1) { e1.printStackTrace(); } break;
	 * 
	 * case "jpg": case "png": try { ImgRotoscoper2 go = new ImgRotoscoper2(in, mc,
	 * detectionAlgorithm,sigmaValue,kernelSizeValue); new Thread(go).start(); }
	 * catch (Exception e1) { e1.printStackTrace(); } break;
	 * 
	 * case "mov": case "mp4": try { Mp4Rotoscoper go = new Mp4Rotoscoper( in, mc,
	 * detectionAlgorithm, out,sigmaValue,kernelSizeValue); new Thread(go).start();
	 * } catch (Exception e1) { e1.printStackTrace(); } break;
	 * 
	 * default: mc.ConsoleError("Error: input valid file");
	 * 
	 * System.out.println(extention); } } catch (Exception e1) {
	 * mc.ConsoleError("Error: fill in all required feilds"); }
	 * 
	 * }
	 * 
	 * 
	 * public void renderCanny(double sigmaValue,int kernelSizeValue,int
	 * placeholder) {
	 * 
	 * try {
	 * 
	 * 
	 * String detectionAlgorithm =
	 * algorithmTypeGroup.getSelection().getActionCommand(); // int
	 * edgeDistanceValue = Integer.parseInt(edgeDistanceTxt.getText()); //int
	 * edgeDistanceValue = 1; double sigma = 1;
	 * 
	 * String extention = in.toString().substring(in.toString().lastIndexOf(".") +
	 * 1, in.toString().length());
	 * 
	 * switch (extention) {
	 * 
	 * case "gif": try { //PictureTester go = new PictureTester(edgeDistanceValue,
	 * in, mc); } catch (Exception e1) { e1.printStackTrace(); } break;
	 * 
	 * case "jpg": case "png": try { ImgRotoscoper2 go = new ImgRotoscoper2(in, mc,
	 * detectionAlgorithm,sigmaValue,kernelSizeValue); new Thread(go).start(); }
	 * catch (Exception e1) { e1.printStackTrace(); } break;
	 * 
	 * case "mov": case "mp4": try { Mp4Rotoscoper go = new Mp4Rotoscoper( in, mc,
	 * detectionAlgorithm, out,sigmaValue,kernelSizeValue); new Thread(go).start();
	 * } catch (Exception e1) { e1.printStackTrace(); } break;
	 * 
	 * default: mc.ConsoleError("Error: input valid file");
	 * 
	 * System.out.println(extention); } } catch (Exception e1) {
	 * mc.ConsoleError("Error: fill in all required feilds"); }
	 * 
	 * }
	 */

	public void updateRequestSimple(int edgeDistanceValue) throws InterruptedException, ExecutionException {

		String detectionAlgorithm = algorithmTypeGroup.getSelection().getActionCommand();

		String extention = in.toString().substring(in.toString().lastIndexOf(".") + 1, in.toString().length());

		
		
		previewPanel.isLoading = true;
		previewPanel.repaint();

		imageProcess.setOperationSimple(edgeDistanceValue);
		//Future<BufferedImage[]> future = executor.submit(imageProcess);
		Thread process=new Thread(imageProcess);
		process.start();
		//previewPanel.setImage(imageProcess.get()[0]);
		previewPanel.isLoading = false;
		previewPanel.repaint();
		
		

	}

	public void updateRequestCanny(double sigma, int kernelSize) {

		String detectionAlgorithm = algorithmTypeGroup.getSelection().getActionCommand();

		String extention = in.toString().substring(in.toString().lastIndexOf(".") + 1, in.toString().length());


			previewPanel.isLoading = true;
			previewPanel.repaint();

			
			imageProcess.setOperationCanny(sigma, kernelSize);
			Thread process=new Thread(imageProcess);
			process.start();
			previewPanel.isLoading = false;
			previewPanel.repaint();
			

		
	}

	public void updateRequestSobel(double sigma, int kernelSize) {

		String detectionAlgorithm = algorithmTypeGroup.getSelection().getActionCommand();

		String extention = in.toString().substring(in.toString().lastIndexOf(".") + 1, in.toString().length());

			previewPanel.isLoading = true;
			previewPanel.repaint();


			imageProcess.setOperationSobel(sigma, kernelSize);
			Thread process=new Thread(imageProcess);
			process.start();
			previewPanel.isLoading = false;
			previewPanel.repaint();
			
	}

	public static void main(String[] args) throws Exception {

		// new RotoScape();
		Runnable doCreateAndShowGUI = new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		};
		SwingUtilities.invokeLater(doCreateAndShowGUI);
	}
}
