import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.AdjustmentListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.ScrollPaneLayout;
import javax.swing.SwingUtilities;

public class imagePreviewPanel extends JPanel {

	protected BufferedImage[] previewImages;
	public boolean isLoading;
	protected int selectedImage = 0;
	protected dockableTimeLine linkedTimeLine;
	protected BufferedImage image;
	protected ImageIcon icon;
	protected ImageCanvas canvas = new ImageCanvas();
	protected JLabel lbl;
	protected double zoomFactor = 1.0;
	protected JScrollPane scrollPane;

imagePreviewPanel(){
	super(new BorderLayout());
	//this.setBackground(Color.GREEN);
	
	 scrollPane=new JScrollPane(canvas);
	
	//this.setLayout(new ScrollPaneLayout());
	this.setBackground(Color.blue);
	
	//canvas.setPreferredSize(new Dimension(400,400));
	scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
	scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
	
	scrollPane.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
		@Override
		 public void adjustmentValueChanged(java.awt.event.AdjustmentEvent ae){
			
			canvas.repaint();
		}
		
	});
	
	
	scrollPane.getHorizontalScrollBar().addAdjustmentListener(new AdjustmentListener() {
		@Override
		 public void adjustmentValueChanged(java.awt.event.AdjustmentEvent ae){
			
			canvas.repaint();
		}
		
	});
	scrollPane.setVisible(true);
	canvas.setParentViewPort(scrollPane.getViewport());
	this.add(scrollPane,BorderLayout.CENTER);
	
	
}

	

	public void setImage(BufferedImage[] previewImages) {

		this.previewImages = previewImages;
		linkedTimeLine.setFrameArraySize(previewImages.length);
		canvas.setImage(previewImages[selectedImage]);
		canvas.repaint();
		canvas.revalidate();
		scrollPane.revalidate();
		scrollPane.repaint();
		this.repaint();
	}

	public void setSelectedImage(int in) {
		this.selectedImage = in;
		image = previewImages[selectedImage];
		canvas.setImage(previewImages[selectedImage]);
		canvas.repaint();
		scrollPane.revalidate();
		//scrollPane.repaint();
		this.repaint();

	}

	public void setLinkedTimeLine(dockableTimeLine in) {
		this.linkedTimeLine = in;
	}

}
