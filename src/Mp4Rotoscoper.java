import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import javax.imageio.ImageIO;
import javax.swing.JTextArea;

import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.Java2DFrameConverter;
import org.bytedeco.javacv.FrameGrabber.Exception;
import org.bytedeco.opencv.opencv_core.*;
import org.bytedeco.opencv.opencv_imgproc.*;
import org.jcodec.api.SequenceEncoder;
import org.jcodec.api.awt.AWTSequenceEncoder;
import org.jcodec.codecs.h264.H264Encoder;
import org.jcodec.common.Codec;
import org.jcodec.common.Format;
import org.jcodec.common.io.NIOUtils;
import org.jcodec.common.model.Rational;
import org.jcodec.scale.AWTUtil;

import com.google.common.io.Files;

public class Mp4Rotoscoper extends rotoscopeUtil {

	
	/**
	 * Constructor for a video with simple edge detection
	 * 
	 * @param in
	 * @param mc
	 * @param out
	 * @param edge
	 * @param sigma
	 * @param kernelSize
	 * @throws Exception
	 * @throws IOException
	 */

	public Mp4Rotoscoper(File in, MessageConsole mc,imagePreviewPanel previewPanel) throws Exception, IOException {
		super(in, mc,previewPanel);
		
		
		// TODO this.outputImage=new Picture(ImageIO.read(in));
	}

	private void nameOutputFile(int ed) throws java.lang.Exception {

		mc.ConsolePrintLn(input.getPath());
		mc.ConsolePrintLn(input.getParent());
		String parentdir = input.getParentFile().toString();
		String fileName = input.getName();
		fileName = fileName.substring(0, fileName.indexOf("."));
		mc.ConsolePrintLn(fileName);

		File fi = new File(parentdir + "\\" + fileName + "Roto.mp4");

		
		mc.ConsolePrintLn("*Completed input");

		switch (detectionAlgorithm) {

		case "Simple":
			simpleMp4(fi);
			break;

		case "Sobel":
			sobelMp4(input.getPath(), input.getParent(), fi);
			break;

		case "NoFilter":
			noFilterMp4();
			break;
			
		case "Open":
			openMp4();
			break;
			
		default:
			mc.ConsoleError("Error: unknown algorithm type");

		}

	}

	private void openMp4() {
		try {
		Java2DFrameConverter converter = new Java2DFrameConverter();
		FFmpegFrameGrabber frameGrabber = new FFmpegFrameGrabber(input);
		frameGrabber.setFormat("mp4");
		frameGrabber.start();
		outputImage = new BufferedImage[frameGrabber.getLengthInFrames() - 1];
		BufferedImage binStore=new BufferedImage(500, 500, BufferedImage.TYPE_INT_ARGB);
		
		for (int frameIndex = 0; frameIndex < frameGrabber.getLengthInFrames() - 1; frameIndex+=1) {
			frameGrabber.setFrameNumber(frameIndex);
			Frame frame=frameGrabber.grabImage();
			BufferedImage bi = converter.convert(frame);
			bi=deepCopy(bi);
			if (bi != null) {
				outputImage[frameIndex] = bi;
				
				binStore=bi;
			}
			else {
				outputImage[frameIndex]=binStore;
				
			}
			
		}
		
		frameGrabber.stop();
		frameGrabber.close();
	//	publish(outputImage[0]);
		}
		catch(Exception e) {
			e.printStackTrace();
		}

	}
	
	private BufferedImage deepCopy(BufferedImage bi) {
        ColorModel cm = bi.getColorModel();
        boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
        WritableRaster raster = bi.copyData(null);
        return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
    }

	private void simpleMp4(File fi) {
		
		for(int i=0;i< outputImage.length;i++) {
			Picture san=new Picture(outputImage[i]);
			san.edgeDetection2(edge);
			outputImage[i]=san.getBufferedImage();
		}
	}
	
	
	private void processMp4(String in, String imagePath, File file, int ed) throws IOException, Exception {

		long StartTime = System.currentTimeMillis();
		// mc.ConsolePrintLn(System.currentTimeMillis()/1000);

		BufferedImage BinStore = new BufferedImage(500, 500, BufferedImage.TYPE_INT_ARGB);
		Graphics2D ig2 = BinStore.createGraphics();

		ig2.setBackground(Color.BLUE);
		// ig2.clearRect(0, 0, 500, 500);

		Java2DFrameConverter converter = new Java2DFrameConverter();
		FFmpegFrameGrabber frameGrabber = new FFmpegFrameGrabber(in);
		SequenceEncoder mEncoder = new SequenceEncoder(NIOUtils.writableChannel(file), Rational.R(30, 1), Format.MOV,
				Codec.H264, null);
		frameGrabber.start();
		Frame frame;

		double frameRate = frameGrabber.getFrameRate();
		mc.ConsolePrintLn(
				"Video has " + frameGrabber.getLengthInFrames() + " frames and has frame rate of " + frameRate + " ");
		mc.ConsolePrintLn("0/" + frameGrabber.getLengthInFrames() + " ");
		try {
			for (int ii = 0; ii <= frameGrabber.getLengthInFrames() - 1; ii += 1) {
				frameGrabber.setFrameNumber(ii);
				frame = frameGrabber.grabImage();
				BufferedImage bi = converter.convert(frame);
				if (bi != null) {

					Picture san = null;
					san = new Picture(bi);

					san.edgeDetection2(ed);

					BinStore = san.getBufferedImage();
					mc.ConsoleTextUpdate(ii + "/" + frameGrabber.getLengthInFrames() + " ");
					mc.repaint();

					mEncoder.encodeNativeFrame(AWTUtil.fromBufferedImageRGB(bi));

				} else {
					// mc.ConsolePrintLn(ii+"/"+frameGrabber.getLengthInFrames()+" ");
					mEncoder.encodeNativeFrame(AWTUtil.fromBufferedImageRGB(BinStore));
				}

			}
			frameGrabber.stop();
			frameGrabber.close();
			mEncoder.finish();
			mc.ConsolePrintLn("*Completed algorithm");
		} catch (Exception e) {
			e.printStackTrace();
		}

		long EndTime = (System.currentTimeMillis());
		float Sec = (EndTime - StartTime) / 1000f;
		float Min = Sec / 60f;
		mc.ConsolePrintLn("Operation Completed in " + (Min) + " min");

	}

	private void sobelMp4(String in, String imagePath, File file) throws IOException, Exception {

		long StartTime = System.currentTimeMillis();
		// mc.ConsolePrintLn(System.currentTimeMillis()/1000);

		BufferedImage BinStore = new BufferedImage(500, 500, BufferedImage.TYPE_INT_ARGB);
		Graphics2D ig2 = BinStore.createGraphics();

		ig2.setBackground(Color.BLUE);
		// ig2.clearRect(0, 0, 500, 500);

		Java2DFrameConverter converter = new Java2DFrameConverter();
		FFmpegFrameGrabber frameGrabber = new FFmpegFrameGrabber(in);
		SequenceEncoder mEncoder = new SequenceEncoder(NIOUtils.writableChannel(file), Rational.R(30, 1), Format.MOV,
				Codec.H264, null);
		frameGrabber.start();
		Frame frame;
		double frameRate = frameGrabber.getFrameRate();
		mc.ConsolePrintLn(
				"Video has " + frameGrabber.getLengthInFrames() + " frames and has frame rate of " + frameRate + " ");
		mc.ConsolePrintLn("0/" + frameGrabber.getLengthInFrames() + " ");
		try {
			for (int ii = 0; ii <= frameGrabber.getLengthInFrames() - 1; ii += 1) {
				frameGrabber.setFrameNumber(ii);
				frame = frameGrabber.grabImage();
				BufferedImage bi = converter.convert(frame);
				if (bi != null) {

					Picture san = null;
					san = new Picture(bi);

					san.sobel();

					BinStore = san.getBufferedImage();
					mc.ConsoleTextUpdate(ii + "/" + frameGrabber.getLengthInFrames() + " ");
					mc.repaint();

					mEncoder.encodeNativeFrame(AWTUtil.fromBufferedImageRGB(bi));

				} else {
					// mc.ConsolePrintLn(ii+"/"+frameGrabber.getLengthInFrames()+" ");
					mEncoder.encodeNativeFrame(AWTUtil.fromBufferedImageRGB(BinStore));
				}

			}
			frameGrabber.stop();
			frameGrabber.close();
			mEncoder.finish();
			mc.ConsolePrintLn("*Completed algorithm");
		} catch (Exception e) {
			e.printStackTrace();
		}

		long EndTime = (System.currentTimeMillis());
		float Sec = (EndTime - StartTime) / 1000f;
		float Min = Sec / 60f;
		mc.ConsolePrintLn("Operation Completed in " + (Min) + " min");

	}

	private void noFilterMp4() {
		openMp4();
	}


	

	
	/*@Override
	protected BufferedImage[] doInBackground() throws Exception {
		try {
			System.out.println("Started background task: "+detectionAlgorithm); //TODO
			nameOutputFile(edge);
			
			
			
			return outputImage;
		} catch (java.lang.Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	protected void process(List<BufferedImage> chunks) {
		System.out.println("run task"); //TODO
		previewPanel.setImage(chunks.get(0));
		previewPanel.repaint();
	}
	
	@Override
	protected void done() {
		System.out.println("Completed Task: "+detectionAlgorithm); //TODO
		BufferedImage[] outarray=(BufferedImage[]) outbuf.toArray(new BufferedImage[outbuf.size()-1]);
		previewPanel.setImage(outarray);
		previewPanel.setBackground(Color.red);
		previewPanel.repaint();
		
	}*/
	
	@Override
	public void run() {
		try {
			System.out.println("Started background task: "+detectionAlgorithm); 
			nameOutputFile(edge);
			System.out.println("Completed Task: "+detectionAlgorithm); 
			//BufferedImage[] outarray=(BufferedImage[]) outbuf.toArray(new BufferedImage[outbuf.size()-1]);
			previewPanel.setImage(outputImage);
			previewPanel.setBackground(Color.red);
			previewPanel.repaint();
			
		}
		catch(java.lang.Exception e) {
			e.printStackTrace();
		}
	}

}